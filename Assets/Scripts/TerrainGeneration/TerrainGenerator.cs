using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

public class TerrainGenerator : MonoBehaviour {
	public enum DrawMode {NoiseMap, ColorMap, Mesh, FalloffMap};
	public DrawMode drawMode;

	public NoiseGenerator.NormalizeMode normalizeMode;

	// public const int terrainChunkSize = 239; // Unity vertex per mesh limit: 255**2 = 65025 -> 239 (+2 +1) has the nice property of being divisible by even numbers:  2, 4, 6, 8, 10, 12
	// public const int terrainChunkSize = 95; // Flat shading use a lot more vertices

	public bool useFlatShading;

	[Range(0, 6)] // range * 2 = 1, 2, 4, 6, 8, 10, 12 -> nice property values
	public int editorPreviewLOD;

	public float noiseScale;

	public int octaves;

	[Range(0, 1)]
	public float persistance; // Amplitude = persistance ** index -> persistance = [0, 1]
	
	public float lacunarity; // Frecuency = lacunarity ** index -> lacunarity < 1

	public int seed;
	public Vector2 offset;
	public bool useFalloffMap;

	public float meshHeightMultiplier;
	public AnimationCurve meshHeightCurve;

	public bool autoUpdate;

	public TerrainType[] regions;
	static TerrainGenerator instance;

	float[,] falloffMap;

	Queue <TerrainThreadInfo<TerrainData>> terrainDataThreadInfoQueue = new Queue <TerrainThreadInfo<TerrainData>> ();
	Queue <TerrainThreadInfo<MeshData>> meshDataThreadInfoQueue = new Queue <TerrainThreadInfo<MeshData>> ();

	void Awake () {
		falloffMap = FalloffGenerator.GenerateFalloffMap(terrainChunkSize);
	}

	public static int terrainChunkSize {
		get {
			if (instance == null) {
				instance = FindObjectOfType<TerrainGenerator>();
			}
			if (instance.useFlatShading) {
				return 95;
			} else {
				return 239;
			}
		}
	} 

	void Update () {
		if (terrainDataThreadInfoQueue.Count > 0) {
			for (int i = 0; i < terrainDataThreadInfoQueue.Count; i++) {
				TerrainThreadInfo<TerrainData> terrainThreadInfo = terrainDataThreadInfoQueue.Dequeue();
				terrainThreadInfo.callback(terrainThreadInfo.parameter);
			}
		}

		if (meshDataThreadInfoQueue.Count > 0) {
			for (int i = 0; i < meshDataThreadInfoQueue.Count; i++) {
				TerrainThreadInfo<MeshData> terrainThreadInfo = meshDataThreadInfoQueue.Dequeue();
				terrainThreadInfo.callback(terrainThreadInfo.parameter);
			}
		}
		
	}

	public void DrawTerrainInEditor() {
		TerrainData terrainData = GenerateTerrainData(Vector2.zero);

		TerrainDisplay display = FindObjectOfType<TerrainDisplay> ();
		if(drawMode == DrawMode.NoiseMap){
			display.DrawTexture (TextureGenerator.TextureFromHeightMap(terrainData.heightMap));
		} else if(drawMode == DrawMode.ColorMap) {
			display.DrawTexture (TextureGenerator.TextureFromColorMap(terrainData.colorMap, terrainChunkSize, terrainChunkSize));
		} else if(drawMode == DrawMode.Mesh) {
			display.DrawMesh (
				MeshGenerator.GenerateTerrainMesh(terrainData.heightMap, meshHeightMultiplier, meshHeightCurve, editorPreviewLOD, useFlatShading),
				TextureGenerator.TextureFromColorMap(terrainData.colorMap, terrainChunkSize, terrainChunkSize)
			);
		} else if(drawMode == DrawMode.FalloffMap) {
			display.DrawTexture(TextureGenerator.TextureFromHeightMap(FalloffGenerator.GenerateFalloffMap(terrainChunkSize)));
		}
	}

	public void RequestTerrainData (Vector2 centre, Action<TerrainData> callback) {
		ThreadStart threadStart = delegate {
			TerrainDataThread(centre, callback);
		};

		new Thread(threadStart).Start();
	}

	void TerrainDataThread(Vector2 centre, Action<TerrainData> callback) {
		TerrainData terrainData = GenerateTerrainData(centre);
		lock (terrainDataThreadInfoQueue) {
			terrainDataThreadInfoQueue.Enqueue(new TerrainThreadInfo<TerrainData>(callback, terrainData));
		}

	}

	public void RequestMeshData (TerrainData terrainData, int lod, Action<MeshData> callback) {
		ThreadStart threadStart = delegate {  
			MeshDataThread(terrainData, lod, callback);
		};

		new Thread(threadStart).Start();
	}

	void MeshDataThread (TerrainData terrainData, int lod, Action<MeshData> callback) {
		MeshData meshData = MeshGenerator.GenerateTerrainMesh(terrainData.heightMap, meshHeightMultiplier, meshHeightCurve, lod, useFlatShading);
		lock (meshDataThreadInfoQueue) {
			meshDataThreadInfoQueue.Enqueue(new TerrainThreadInfo<MeshData>(callback, meshData));
		}

	}

	TerrainData GenerateTerrainData(Vector2 centre) {
		float[,] noiseMap = NoiseGenerator.GenerateNoiseMap(terrainChunkSize + 2, terrainChunkSize + 2, seed, noiseScale, octaves, persistance, lacunarity, centre + offset, normalizeMode);

		Color[] colorMap = new Color[terrainChunkSize * terrainChunkSize];
		for(int y = 0; y < terrainChunkSize; y++){
			for(int x = 0; x < terrainChunkSize; x++){
				if (useFalloffMap) {
					noiseMap[x, y] = Mathf.Clamp01(noiseMap[x, y] - falloffMap[x, y]);
				}
				float currentHeight = noiseMap[x, y];
				for (int i = 0; i < regions.Length; i++) {
					if (currentHeight >= regions[i].height) {
						colorMap[y * terrainChunkSize + x] = regions[i].color;
					} else {
						break;
					}
				}
			}
		}

		return new TerrainData(noiseMap, colorMap);
	}

	void OnValidate(){
		if (lacunarity < 1){
			lacunarity = 1;
		}
		if (octaves < 0){
			octaves = 0;
		}
		falloffMap = FalloffGenerator.GenerateFalloffMap(terrainChunkSize);
	}

	struct TerrainThreadInfo<T> {
		public readonly Action<T> callback;
		public readonly T parameter;

		public TerrainThreadInfo(Action<T> callback, T parameter) {
			this.callback = callback;
			this.parameter = parameter;
		}

	}
}

[System.Serializable]
public struct TerrainType {
	public string name;
	public float height;
	public Color color;
}

public struct TerrainData {
	public readonly float[,] heightMap;
	public readonly Color[] colorMap;

	public TerrainData (float[,] heightMap, Color[] colorMap) {
		this.heightMap = heightMap;
		this.colorMap = colorMap;
	}
}