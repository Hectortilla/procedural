using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGenerator : MonoBehaviour {

    [SerializeField]
    int x_floor_dimension = 11;
    [SerializeField]
    int z_floor_dimension = 11;
    [SerializeField]
    int n_of_floors = 4;

    [SerializeField]
    WallInfo wall_game_object;
    [SerializeField]
    WallInfo ground_game_object;

    void Start () {
        GenerateBuilding();
    }

    public void GenerateBuilding () {
        for (int floor_number = 0; floor_number < n_of_floors; floor_number++) {

            // TODO: create empty GameObject as a parent of all the floors/walls initializations,
            // this game object will represent the building.
            // TODO: this information (floor_grid: the information of every floor)
            // should be persisted within the building GameObject for later usage
            bool[,] floor_grid = GridGenerator.GenerateGrid(x_floor_dimension, z_floor_dimension);

            // TODO: Initialize only when player enters the building
            FloorGenerator.InstantiateFloor(
                floor_grid,
                floor_number,
                wall_game_object, ground_game_object
            );
        }
    }

}
