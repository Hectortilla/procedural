## Structure

The entry point Component for generating the building is the script [BuildingGenerator.cs](https://gitlab.com/Hectortilla/procedural/blob/master/Assets/Scripts/BuildingGeneration/BuildingGenerator.cs), attach it to an empty GameObject.

It uses [FloorGenerator.cs]((https://gitlab.com/Hectortilla/procedural/blob/master/Assets/Scripts/BuildingGeneration/FloorGenerator.cs)) which is responsable of initializating every floor separatelly.

The data structure that represents the building and its rooms is initialized by [GridGenerator.cs](https://gitlab.com/Hectortilla/procedural/blob/master/Assets/Scripts/BuildingGeneration/GridGenerator.cs).

The different functions that determine the possible configurations of the floor's rooms are decalred in [GridFunctionsGenerator.cs](https://gitlab.com/Hectortilla/procedural/blob/master/Assets/Scripts/BuildingGeneration/GridFunctionsGenerator.cs).

[WallInfo.cs](https://gitlab.com/Hectortilla/procedural/blob/master/Assets/Scripts/BuildingGeneration/WallInfo.cs) contains all the information about the wall as a minimum building block. It stores the GameObject to instantiate as the wall.

## TODOs:

- [x] Basic class structure.
- [x] Different floors configurations.
- [ ] Group all walls and floors of a building in a single GameObject.
- [ ] Make configurable the filling function for every floor.
- [ ] Persist the data of the building on this GameObject.
- [ ] The buildings should be generated at certain range from the player, destroying those that fall out that range.
- [ ] The interior of the buildings should be generated only when the player enters the building.
- [ ] Study how [Wave Function Collapse](https://github.com/mxgmn/WaveFunctionCollapse) would be beneficial for this project.