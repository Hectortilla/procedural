﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class GridGenerator {
    static public bool[,] GenerateGrid(int x_floor_dimension, int z_floor_dimension) {
        bool[,] grid = new bool[x_floor_dimension, z_floor_dimension];

        FillGrid(grid);

        return grid;
    }

    static public void FillGrid (bool[,] grid) {
        // TODO: make configurable the filling function for every floor
        GridFunctionsGenerator.RandomFill(grid);
    }
}
