﻿using System;
using UnityEngine;

public static class FloorGenerator {

    static public void _InstantiateFront (WallInfo wall_info, int n_floor, int x, int z) {
        Vector3 rot = new Vector3(0, 90, 0);

        GameObject.Instantiate(
            wall_info.wall_game_object,
            new Vector3(
                x * wall_info.x_size + (wall_info.x_size/2),
                n_floor * wall_info.y_size,
                z * wall_info.x_size - (wall_info.x_size/2)
            ),
            Quaternion.Euler(rot)
        );

    }

    static public void _InstantiateSide (WallInfo wall_info, int n_floor, int x, int z) {

        GameObject.Instantiate(
            wall_info.wall_game_object,
            new Vector3(
                x * wall_info.x_size,
                n_floor * wall_info.y_size,
                z * wall_info.x_size
            ),
            Quaternion.identity
        );

    }

    static public void _InstantiateGrounds (bool[,] grid, int n_floor, WallInfo ground_info, float y_size) { // No dependent on grid
        
        for (int x = 0; x < grid.GetLength(0); x++) {
            for (int z = 0; z < grid.GetLength(1); z++) {
                Vector3 rot = new Vector3(0, 0, 90);

                GameObject.Instantiate(
                    ground_info.wall_game_object,
                    new Vector3(
                        x * ground_info.x_size + (ground_info.x_size/2),
                        (n_floor * y_size) - (y_size/2),
                        z * ground_info.x_size
                    ),
                    Quaternion.Euler(rot)
                );

            }
        }

    }

    static public void _InstantiateBorders (bool[,] grid, int n_floor, WallInfo wall_info, float y_size) { // No dependent on grid

        for (int x = 0; x < grid.GetLength(0); x++) {
            _InstantiateFront(wall_info, n_floor, x, 0);
            _InstantiateFront(wall_info, n_floor, x, grid.GetLength(1));
        }

        for (int z = 0; z < grid.GetLength(1); z++) {
            _InstantiateSide(wall_info, n_floor, 0, z);
            _InstantiateSide(wall_info, n_floor, grid.GetLength(0), z);
        }

    }


    static public void InstantiateFloor (bool[,] grid, int n_floor, WallInfo wall_info, WallInfo ground_info) {

        for (int x = 0; x < grid.GetLength(0); x++) {
            for (int z = 0; z < grid.GetLength(1); z++) {
                if (z + 1 < grid.GetLength(1) && grid[x, z] != grid[x, z + 1]){
                    _InstantiateFront(wall_info, n_floor, x, z + 1);
                }
                if (x + 1 < grid.GetLength(0) && grid[x, z] != grid[x + 1, z]){
                    _InstantiateSide(wall_info, n_floor, x + 1, z);
                }
            }
        }

        _InstantiateGrounds(grid, n_floor, ground_info, wall_info.y_size);
        _InstantiateBorders(grid, n_floor, wall_info, wall_info.y_size);

    }
}
