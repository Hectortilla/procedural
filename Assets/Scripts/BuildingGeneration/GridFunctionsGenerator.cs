﻿using UnityEngine;

public static class GridFunctionsGenerator {

    // It generates 4 rooms crossed by two corridors.
    public static void CrossedRooms(bool[,] grid) {
        for (int x = 0; x < grid.GetLength(0); x++)
        {
            for (int z = 0; z < grid.GetLength(1); z++)
            {
                if(x == Mathf.FloorToInt(grid.GetLength(0)/2) || z == Mathf.FloorToInt(grid.GetLength(1)/2)){
                    grid[x,z] = true;
                }
            }
        }
    }

    // It generates random rooms.
    public static void RandomFill(bool[,] grid) {
        for (int x = 0; x < grid.GetLength(0); x++)
        {
            for (int z = 0; z < grid.GetLength(1); z++)
            {
                grid[x,z] = Random.Range(0, 2) == 1;
            }
        }
    }

    // It generates one room per cell.
    public static void Filled(bool[,] grid) {
        for (int x = 0; x < grid.GetLength(0); x++)
        {
            for (int z = 0; z < grid.GetLength(1); z++)
            {
                if (x == 0 && z == 0){
                    grid[x,z] = true;
                } else if (x == 0){
                    grid[x,z] = !grid[x,z - 1];
                } else {
                    grid[x,z] = !grid[x-1,z];
                }
            }
        }
    }

    // It generates an empty floor.
    public static void Empty(bool[,] grid) {
        for (int x = 0; x < grid.GetLength(0); x++)
        {
            for (int z = 0; z < grid.GetLength(1); z++)
            {
                grid[x,z] = false;
            }
        }
    }
}
