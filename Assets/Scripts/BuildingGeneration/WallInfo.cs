﻿using UnityEngine;

public class WallInfo : MonoBehaviour {
	public GameObject wall_game_object;
	public float x_size; // Size of the wall on the x axis, I should also make an offset for the thickness [z axis]
	public float y_size; // Size of the wall on the y axis, I should also make an offset for the thickness [z axis]


    public WallInfo(GameObject _wall_game_object, float _x_size, float _y_size) {
        wall_game_object = _wall_game_object;
        x_size = _x_size;
        y_size = _y_size;
    }
}
