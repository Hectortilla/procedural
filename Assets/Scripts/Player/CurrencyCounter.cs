﻿using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyCounter : MonoBehaviour {
    [SerializeField]
    private Text currencyUIText;
    [SerializeField]
    private int currency = 0;
    
    void Start () {
       updateUIcurrencyCounter();
    }
    public void loseCurrency(){
        currency = 0;
        updateUIcurrencyCounter();
    }
    public bool applyCurrency(int currencyChange){
        if (currency + currencyChange < 0){
            return false;
        }
        currency += currencyChange;
        updateUIcurrencyCounter();
        return true;
    }

    public void updateUIcurrencyCounter(){
       currencyUIText.text = currency.ToString();
    }
}
