﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class PlayerLife : MonoBehaviour {
	[SerializeField]
	private Text healthUIText;
	
	[SerializeField]
	GameObject spawnPoint;
	CurrencyCounter currencyCounter;
	Inventory inventory;

	[SerializeField]
	RawImage bloodScreen;

	private int life = 100;

	void Start () {
		currencyCounter = FindObjectsOfType<CurrencyCounter>()[0];
		inventory = GetComponent<Inventory>();
		updateUIhealthCounter();
	}

	public void applyDamage(int damage){
		if (damage < 0){
			StopAllCoroutines();
			IEnumerator coroutine = applyBloodScreen();
	        StartCoroutine(coroutine);
		}
		life += damage;
		updateUIhealthCounter();
		if (life <= 0){
			kill();
		}
	}
	private IEnumerator applyBloodScreen(){
		float alphaValue = 1f;
		while (alphaValue > 0){
			alphaValue -= 0.025f;
			Color temp = bloodScreen.color;
			temp.a=alphaValue;
			bloodScreen.color = temp;
			yield return new WaitForSeconds(0.05f);
		}
	}
	public void kill(){
		transform.position = spawnPoint.transform.position;
		life = 100;
		GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
		currencyCounter.loseCurrency();
		inventory.EmptyInvcentory();
		updateUIhealthCounter();
	}
	public void updateUIhealthCounter(){
		healthUIText.text = life.ToString();
	}
}
