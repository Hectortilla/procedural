﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageToPlayer : MonoBehaviour {
	private int damage = 34;
    void OnCollisionEnter (Collision col)
    {
        if(col.gameObject.GetComponent<PlayerLife>() != null)
        {
        	if (Mathf.Abs(col.impulse.x) > 1000 || Mathf.Abs(col.impulse.y) > 1000 || Mathf.Abs((col.impulse.z)) > 1000){
        		col.gameObject.GetComponent<PlayerLife>().kill();
        	} else {
        		col.gameObject.GetComponent<PlayerLife>().applyDamage(-damage);
        	}
        }
    }
}
