﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyAwarder : MonoBehaviour {
    private bool currencyGiven = false;
    void OnCollisionEnter (Collision col)
    {
        if (
            !currencyGiven &&
            col.relativeVelocity.magnitude > 1 &&
            (col.gameObject.layer != 8 || col.gameObject.transform.parent == null)){
                DestroyAndReward();
        }
    }
    void DestroyAndReward (){
        CurrencyCounter currencyCounter = FindObjectsOfType<CurrencyCounter>()[0];
        currencyCounter.applyCurrency(1);
        Destroy(gameObject, 2);
        currencyGiven = true;
    }
}
