﻿using UnityEngine;
using UnityEditor;

[CustomEditor (typeof (BuildingGenerator))]
public class BuildingGeneratorEditor : Editor {

	public override void OnInspectorGUI() {
		BuildingGenerator buildingGenerator = (BuildingGenerator)target;

		if (DrawDefaultInspector ()) {
			buildingGenerator.GenerateBuilding();
		}

		if (GUILayout.Button ("Generate")) {
			buildingGenerator.GenerateBuilding();
		}
	}
}