﻿using System.Collections;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof (TerrainGenerator))]
public class TerrainGeneratorEditor : Editor {

	public override void OnInspectorGUI() {
		TerrainGenerator terrainGen = (TerrainGenerator)target;

		if (DrawDefaultInspector ()) {
			if (terrainGen.autoUpdate) {
				terrainGen.DrawTerrainInEditor ();
			}
		}

		if (GUILayout.Button ("Generate")) {
			terrainGen.DrawTerrainInEditor ();
		}
}
}
