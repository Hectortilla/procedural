# Procedural

Some procedural generation experiments made in Unity3D.

This project it is under construction and in a very early stage, this means is very prone to suffer drastic changes it its code structure.

## Getting Started

To get started it should be as simple as cloning this repo and opening the resulting folder in the game engine Unity. Unity will set the rest for you. Unity may warn you about version conflict, it should not be a problem, press 'ok'.

## Content

So far this project contains two parts: One for generating buildings procedurally, and the other for generating terrain procedurally.

### Procedural buildings [Assets/Scripts/BuildingGeneration](https://gitlab.com/Hectortilla/procedural/tree/master/Assets/Scripts/BuildingGeneration)

Under construction. This part of the code is made all by myself, the intention is to procedurally generate buildings with rooms inside.
<br /><br />Click on the link to read its README.md
### Procedural terrain [Assets/Scripts/TerrainGeneration](https://gitlab.com/Hectortilla/procedural/tree/master/Assets/Scripts/TerrainGeneration)

Under construction. The code it is not mine, I have been folowing a tutorial by [Sebastian Lague](https://www.youtube.com/user/Cercopithecan)

## License

This project is licensed under the MIT License.